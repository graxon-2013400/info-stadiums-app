package com.gerberraxon.infostadiumapp.Adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gerberraxon.infostadiumapp.R;
import com.gerberraxon.infostadiumapp.beans.Stadium;

import com.parse.ParseObject;

import java.util.Date;
import java.util.List;

/**
 * Created by graxon on 24/10/15.
 */
public class FavoritosAdapter extends RecyclerView.Adapter<FavoritosAdapter.ViewHolder> {
    private List<Stadium> itemsData;
    private List<ParseObject> objs_parse_fav;
    static Context ctx;
    Typeface font, font_bold;

    public FavoritosAdapter(Context parentContext, List<Stadium> data, List<ParseObject> objs_fav) {
        this.itemsData = data;
        this.objs_parse_fav = objs_fav;
        this.ctx = parentContext;
        font = Typeface.createFromAsset(ctx.getAssets(), "fonts/Caviar-Dreams/CaviarDreams.ttf");
        font_bold = Typeface.createFromAsset(ctx.getAssets(), "fonts/Caviar-Dreams/Caviar_Dreams_Bold.ttf");
    }

    @Override
    public FavoritosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favoritos_list_item, parent, false);

        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder vholder, int position) {
        final ViewHolder viewHolder = vholder;
        String name = itemsData.get(position).getName();
        String presentation = (name.length() > 20)? name.substring(0, 19) + "..": name;

        viewHolder.tv_name.setText(presentation);
        viewHolder.tv_address.setTypeface(font);
        viewHolder.tv_address.setText(itemsData.get(position).getAddress());
        viewHolder.tv_fav_desde.setTypeface(font_bold);
        Date date_fav = objs_parse_fav.get(getPosition()).getCreatedAt();
        String[] date =  date_fav.toString().split(" ");
        String date_fav_str = date[0] + " " + date[1] + " " + date[2] + "," + date[5].substring(2, 4);
        viewHolder.tv_fav_desde.setText(date_fav_str);

        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setPosition(viewHolder.getPosition());
                return false;
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{

        public TextView tv_name;
        public TextView tv_address;
        public TextView tv_fav_desde;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            tv_name = (TextView) itemLayoutView.findViewById(R.id.tv_name);
            tv_address = (TextView) itemLayoutView.findViewById(R.id.tv_address);
            tv_fav_desde = (TextView) itemLayoutView.findViewById(R.id.tv_fav_desde);

            itemLayoutView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

            menu.setHeaderTitle(itemsData.get(getPosition()).getName());
            menu.add(Menu.NONE, R.id.ctx_action_delete_from_fav, Menu.NONE, R.string.ctx_rcrl_action_delete_from_fav);
        }
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }


    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Stadium getLongClickSelectedStadium(){
        return itemsData.get(position);
    }

    public ParseObject getLongClickSelectedParseObject(){
        return objs_parse_fav.get(position);
    }

    public List<Stadium> getFavoriteStadium(){
        return itemsData;
    }
}