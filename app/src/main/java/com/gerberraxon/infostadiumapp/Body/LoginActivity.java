package com.gerberraxon.infostadiumapp.Body;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gerberraxon.infostadiumapp.R;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import org.w3c.dom.Text;


public class LoginActivity extends ActionBarActivity implements Button.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Typeface ty = Typeface.createFromAsset(getAssets(), "fonts/Walkway/Walkway_Bold.ttf");

        TextView lbl_login_app_name = (TextView) findViewById(R.id.lbl_login_app_name);
        TextView lbl_login_welcome = (TextView) findViewById(R.id.lbl_login_welcome);

        Button btn_registrarse = (Button) findViewById(R.id.btn_registrarse);

        Button btn_login = (Button) findViewById(R.id.btn_login);

        lbl_login_app_name.setTypeface(ty);
        lbl_login_welcome.setTypeface(ty);
        btn_registrarse.setTypeface(ty);
        btn_login.setTypeface(ty);

        btn_registrarse.setOnClickListener(this);
        btn_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_registrarse){
            Intent registro =  new Intent(LoginActivity.this, RegistroActivity.class);

            LoginActivity.this.startActivity(registro);

        }else if(v.getId() == R.id.btn_login){

            EditText et_user = (EditText) findViewById(R.id.tv_login_username);
            EditText et_password = (EditText) findViewById(R.id.tv_login_password);

            ParseUser.logInInBackground(et_user.getText().toString(), et_password.getText().toString(), new LogInCallback() {
                @Override
                public void done(ParseUser user, ParseException e) {
                    if(user != null){
                        Intent intentMain = new Intent(LoginActivity.this, DashBoardActivity.class);

                        startActivity(intentMain);

                        finish();
                    }else{
                        Toast.makeText(LoginActivity.this, "Datos incorrectos.", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
}
