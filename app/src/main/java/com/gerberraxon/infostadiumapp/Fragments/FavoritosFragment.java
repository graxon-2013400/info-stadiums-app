package com.gerberraxon.infostadiumapp.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.gerberraxon.infostadiumapp.Adapters.FavoritosAdapter;
import com.gerberraxon.infostadiumapp.Body.StadiumDetail;
import com.gerberraxon.infostadiumapp.Listeners.RecyclerItemClickListener;
import com.gerberraxon.infostadiumapp.R;
import com.gerberraxon.infostadiumapp.Utils.Util;
import com.gerberraxon.infostadiumapp.beans.Stadium;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritosFragment extends Fragment {

    private RecyclerView mRecyclerViewFav;
    private RecyclerView.Adapter mAdapterFav;
    private RecyclerView.LayoutManager mLayoutManagerFav;

    ParseQuery<ParseObject> query = ParseQuery.getQuery("detalle_favoritos");

    public FavoritosFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_favoritos, container, false);

        mRecyclerViewFav = (RecyclerView) v.findViewById(R.id.rc_favoritos);
        mLayoutManagerFav = new LinearLayoutManager(getActivity());
        mRecyclerViewFav.setLayoutManager(mLayoutManagerFav);

        putData();

        mRecyclerViewFav.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Stadium st = ((FavoritosAdapter) mRecyclerViewFav.getAdapter()).getFavoriteStadium().get(position);
                        HomeFragment.setStadiumselected(st);

                        Intent descriptionIntent = new Intent(getActivity(), StadiumDetail.class);

                        startActivity(descriptionIntent);
                    }
                })
        );
        return v;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ctx_action_delete_from_fav:

                try{
                    ParseObject std = ((FavoritosAdapter) mRecyclerViewFav.getAdapter()).getLongClickSelectedParseObject();

                    ParseObject.createWithoutData("detalle_favoritos", std.getObjectId()).delete();

                    putData();
                }catch (Exception e){
                    Log.e("try delete", e.getMessage());
                }

                break;
        }
        return super.onContextItemSelected(item);
    }

    public void putData(){

        final List<Integer> ids = new ArrayList<>();
        query.whereEqualTo("user", ParseUser.getCurrentUser().getObjectId());
        try{
            List<ParseObject> results = query.find();

            for (ParseObject obj : results) {
                if(obj.get("id_stadium") != null){
                    ids.add(obj.getInt("id_stadium"));
                }
            }

            mAdapterFav = new FavoritosAdapter(getActivity(), Util.getStdsById(ids), results);
            mRecyclerViewFav.setAdapter(mAdapterFav);
        }catch (Exception e){
            Log.e("putData", "Error: " + e.getMessage());
        }

    }

}
