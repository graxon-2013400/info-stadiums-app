package com.gerberraxon.infostadiumapp.Body;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.gerberraxon.infostadiumapp.Fragments.AboutFragment;
import com.gerberraxon.infostadiumapp.Fragments.FavoritosFragment;
import com.gerberraxon.infostadiumapp.Fragments.HomeFragment;
import com.gerberraxon.infostadiumapp.Fragments.NavDrawerFragment;
import com.gerberraxon.infostadiumapp.R;
import com.gerberraxon.infostadiumapp.beans.Stadium;
import com.parse.ParseUser;


public class DashBoardActivity extends ActionBarActivity implements NavDrawerFragment.FragmentDrawerListener{


    private final String INICIO = "Inicio";
    private final String FAVORITOS = "Favoritos";
    private final String ACERCA = "Acerca";

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavDrawerFragment drawer = (NavDrawerFragment)getSupportFragmentManager().findFragmentById(R.id.navigation_drawer_fragmnet);

        drawer.setUp((DrawerLayout) findViewById(R.id.drawer_layout), mToolbar, R.id.navigation_drawer_fragmnet);
        drawer.setDrawerListener(this);

        onDrawerItemSelected(this.getCurrentFocus(), 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dash_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_logout) {
            ParseUser.logOut();

            Intent login = new Intent(this, LoginActivity.class);

            DashBoardActivity.this.startActivity(login);

            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        Fragment content = null;
        String titulo = getString(R.string.app_name);

        switch(position){
            case 1:
                content = new HomeFragment();
                titulo = INICIO;
                break;
            case 2:
                content = new FavoritosFragment();
                titulo = FAVORITOS;
                break;
            case 3:
                content = new AboutFragment();
                titulo = ACERCA;
                break;
            default:
                break;
        }

        if(content != null){
            ((RelativeLayout)findViewById(R.id.ContentLayout)).removeAllViewsInLayout();

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.ContentLayout, content);
            fragmentTransaction.commit();
            getSupportActionBar().setTitle(titulo);
        }
    }
}
