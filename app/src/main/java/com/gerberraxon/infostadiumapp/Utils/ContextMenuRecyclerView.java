package com.gerberraxon.infostadiumapp.Utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.ContextMenu;

/**
 * Created by graxon on 9/11/15.
 */
public class ContextMenuRecyclerView extends RecyclerView {


    private ContextMenu.ContextMenuInfo mContextMenuInfo;

    public ContextMenuRecyclerView(Context context) {
        super(context);
    }

    public ContextMenuRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ContextMenuRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected ContextMenu.ContextMenuInfo getContextMenuInfo() {
        return mContextMenuInfo;
    }

    public void openContextMenu(int position) {
        if (position >= 0) {
            final long childId = getAdapter().getItemId(position);
            mContextMenuInfo = createContextMenuInfo(position, childId);
        }
        showContextMenu();
    }


    ContextMenu.ContextMenuInfo createContextMenuInfo(int position, long id) {
        return new RecyclerContextMenuInfo(position, id);
    }

    public static class RecyclerContextMenuInfo implements ContextMenu.ContextMenuInfo {

        public int position;

        public long id;

        public RecyclerContextMenuInfo(int position, long id) {
            this.position = position;
            this.id = id;
        }
    }
}
