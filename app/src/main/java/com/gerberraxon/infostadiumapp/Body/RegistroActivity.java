package com.gerberraxon.infostadiumapp.Body;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.gerberraxon.infostadiumapp.R;
import com.gerberraxon.infostadiumapp.Utils.Util;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.util.HashMap;


public class RegistroActivity extends ActionBarActivity implements Button.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        Button btn_crear = (Button) findViewById(R.id.btn_crear);
        Button btn_login_back = (Button) findViewById(R.id.btn_login_back);

        btn_crear.setOnClickListener(this);
        btn_login_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_crear){
            View view = this.getWindow().getDecorView();

            HashMap<String, Integer> tvs = new HashMap<>();

            tvs.put("correo", R.id.tv_registro_correo);
            tvs.put("user", R.id.tv_registro_username);
            tvs.put("password", R.id.tv_registro_password);
            tvs.put("password_confirm", R.id.tv_registro_password_confirmar);

            HashMap<String, String> resultado = Util.getTextView_Text(view, tvs);

            String correo = resultado.get("correo");
            String user = resultado.get("user");
            String password = resultado.get("password");
            String password_confirm = resultado.get("password_confirm");

            if(correo.equals("") || user.equals("") || password.equals("") || password_confirm.equals("")){
                Toast.makeText(RegistroActivity.this, R.string.revisar_datos, Toast.LENGTH_LONG).show();
            }else{
                if(password.equals(password_confirm)){
                    ParseUser parseUser = new ParseUser();
                    parseUser.setUsername(user);
                    parseUser.setPassword(password);
                    parseUser.setEmail(correo);

                    parseUser.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Toast.makeText(RegistroActivity.this, R.string.creado, Toast.LENGTH_LONG).show();
                                finish();
                            } else {
                                Toast.makeText(RegistroActivity.this, R.string.usuario_existente, Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                }else{
                    Toast.makeText(RegistroActivity.this, R.string.advertencia_contrasenias, Toast.LENGTH_LONG).show();
                }
            }
        }else if(v.getId() == R.id.btn_login_back){
            Intent login_back = new Intent(RegistroActivity.this, LoginActivity.class);

            RegistroActivity.this.startActivity(login_back);

            finish();
        }
    }
}
