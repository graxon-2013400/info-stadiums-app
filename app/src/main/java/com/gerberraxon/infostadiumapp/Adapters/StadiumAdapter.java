package com.gerberraxon.infostadiumapp.Adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gerberraxon.infostadiumapp.R;
import com.gerberraxon.infostadiumapp.beans.Stadium;

import java.util.List;

/**
 * Created by graxon on 24/10/15.
 */
public class StadiumAdapter extends RecyclerView.Adapter<StadiumAdapter.ViewHolder> {
    private List<Stadium> itemsData;
    static Context ctx;
    Typeface font;

    public StadiumAdapter(Context parentContext, List<Stadium> data) {
        this.itemsData = data;
        this.ctx = parentContext;
        font = Typeface.createFromAsset(ctx.getAssets(), "fonts/Caviar-Dreams/CaviarDreams.ttf");
    }

    @Override
    public StadiumAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.stadium_list_item, parent, false);

        return new ViewHolder(itemLayoutView);
    }

    public void updateData(List<Stadium> items) {
        itemsData.clear();
        itemsData.addAll(items);
        notifyDataSetChanged();
    }
    public void addItem(int position, Stadium item) {
        itemsData.add(position, item);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        itemsData.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder vholder, int position) {
        final ViewHolder viewHolder = vholder;
        String name = itemsData.get(position).getName();
        String presentation = (name.length() > 25)? name.substring(0, 24) + "..": name;

        viewHolder.tv_name.setText(presentation);
        viewHolder.tv_address.setTypeface(font);
        viewHolder.tv_address.setText(itemsData.get(position).getAddress());

        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setPosition(viewHolder.getPosition());
                return false;
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{

        public TextView tv_name;
        public TextView tv_address;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_name = (TextView) itemLayoutView.findViewById(R.id.tv_name);
            tv_address = (TextView) itemLayoutView.findViewById(R.id.tv_address);

            itemLayoutView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            //menuInfo is null
            menu.setHeaderTitle(itemsData.get(getPosition()).getName());
            menu.add(Menu.NONE, R.id.ctx_action_favorite, Menu.NONE, R.string.ctx_rcrl_action_favorite);
        }
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }


    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Stadium getLongClickSelectedStadium(){
        return itemsData.get(position);
    }
}