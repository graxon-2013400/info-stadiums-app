package com.gerberraxon.infostadiumapp.Utils;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gerberraxon.infostadiumapp.beans.Stadium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by graxon on 3/11/15.
 */
public class Util {

    private static List<Stadium> stadiums;

    public static String getTextView_Text(Context context, ViewGroup container, int layout, int id_tv){
        LayoutInflater inflater = LayoutInflater.from(context);

        View v = inflater.inflate(layout, container, true);

        TextView tv = (TextView) v.findViewById(id_tv);

        return tv.getText().toString();
    }

    public static HashMap<String, String> getTextView_Text(View v, HashMap<String, Integer> tvs){
        HashMap<String, String> resultado = new HashMap<>();

        for(Map.Entry<String, Integer> entry : tvs.entrySet()){
            TextView tv = (TextView) v.findViewById(entry.getValue());
            resultado.put(entry.getKey(), tv.getText().toString());
        }

        return resultado;
    }

    /**
     * Metodos de manejo de Estadios
     *
     *
     */
    public static List<Stadium> getStadiums(){
        return stadiums;
    }

    public static void setStadiums(List<Stadium> stds){
        stadiums = stds;
    }

    public static Stadium getStdById(int id){
        for(Stadium std : stadiums){
            if(std.getId() == id) return std;
        }
        return null;
    }

    public static List<Stadium> getStdsById(List<Integer> ids){
        List<Stadium> local = new ArrayList<>();
        for(Stadium std : stadiums){
            if(ids.contains(std.getId())){
                local.add(std);
            }
        }
        return local;
    }
}
