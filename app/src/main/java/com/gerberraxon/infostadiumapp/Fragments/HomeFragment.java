package com.gerberraxon.infostadiumapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.gerberraxon.infostadiumapp.Adapters.NavDrawerAdapter;
import com.gerberraxon.infostadiumapp.Adapters.StadiumAdapter;
import com.gerberraxon.infostadiumapp.Body.StadiumDetail;
import com.gerberraxon.infostadiumapp.Listeners.RecyclerItemClickListener;
import com.gerberraxon.infostadiumapp.R;
import com.gerberraxon.infostadiumapp.Retrofit.StadiumService;
import com.gerberraxon.infostadiumapp.Utils.ContextMenuRecyclerView;
import com.gerberraxon.infostadiumapp.Utils.Util;
import com.gerberraxon.infostadiumapp.beans.Stadium;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private static Stadium stdSelected = new Stadium();

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint("https://s3.amazonaws.com/jon-hancock-phunware/")
                .setClient(new OkClient(new OkHttpClient()));

        RestAdapter adtr = builder.build();

        StadiumService std = adtr.create(StadiumService.class);


        mRecyclerView = (RecyclerView) v.findViewById(R.id.rc_stadiums);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        std.getStadiums(new Callback<List<Stadium>>() {
            @Override
            public void success(List<Stadium> stds, retrofit.client.Response response) {
                Util.setStadiums(stds);
                mAdapter = new StadiumAdapter(getActivity(), Util.getStadiums());
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Retrofit FAILURE", error.getMessage());
                Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent descriptionIntent = new Intent(view.getContext(), StadiumDetail.class);
                        stdSelected = Util.getStadiums().get(position);

                        startActivity(descriptionIntent);
                    }
                })
        );

        registerForContextMenu(mRecyclerView);
        return v;
    }

    public static Stadium getStadiumselected(){
        return stdSelected;
    }

    public static void setStadiumselected(Stadium std){
        stdSelected = std;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        int position = -1;
        try {
            position = ((StadiumAdapter) mRecyclerView.getAdapter()).getPosition();
        } catch (Exception e) {
            Log.d("onContextItemSelected", e.getLocalizedMessage(), e);
            return super.onContextItemSelected(item);
        }
        switch (item.getItemId()) {
            case R.id.ctx_action_share:
                Toast.makeText(getActivity(), "HOLA", Toast.LENGTH_LONG).show();
                break;
            case R.id.ctx_action_favorite:

                Stadium std = ((StadiumAdapter) mRecyclerView.getAdapter()).getLongClickSelectedStadium();
                ParseObject po = new ParseObject("detalle_favoritos");

                po.put("id_stadium", std.getId());
                po.put("user", ParseUser.getCurrentUser().getObjectId());

                po.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        Toast.makeText(getActivity(), "Done", Toast.LENGTH_LONG).show();
                    }
                });
                break;
        }
        return super.onContextItemSelected(item);
    }

}
