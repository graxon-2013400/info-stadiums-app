package com.gerberraxon.infostadiumapp.beans;

import java.util.List;

/**
 * Created by graxon on 24/10/15.
 */
public class Stadium {
    private String zip;
    private String phone;
    private String ticket_link;
    private String state;
    private int pcode;
    private String city;
    private int id;
    private String tollfreephone;
    private List<Schedule> schedule;
    private String address;
    private String image_url;
    private String description;
    private String name;
    private String longitude;
    private String latitude;

    public void setZip(String zip) {
        this.zip = zip;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setTicket_link(String ticket_link) {
        this.ticket_link = ticket_link;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setPcode(int pcode) {
        this.pcode = pcode;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTollfreephone(String tollfreephone) {
        this.tollfreephone = tollfreephone;
    }

    public void setSchedule(List<Schedule> schedule) {
        this.schedule = schedule;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getZip() {

        return zip;
    }

    public String getPhone() {
        return phone;
    }

    public String getTicket_link() {
        return ticket_link;
    }

    public String getState() {
        return state;
    }

    public int getPcode() {
        return pcode;
    }

    public String getCity() {
        return city;
    }

    public int getId() {
        return id;
    }

    public String getTollfreephone() {
        return tollfreephone;
    }

    public List<Schedule> getSchedule() {
        return schedule;
    }

    public String getAddress() {
        return address;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }
}
