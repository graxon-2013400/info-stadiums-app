package com.gerberraxon.infostadiumapp.Application;

import android.app.Application;

import com.gerberraxon.infostadiumapp.R;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;

/**
 * Created by graxon on 29/10/15.
 */
public class ParseApplication extends Application {
    public void onCreate() {
        super.onCreate();
        Parse.initialize(this, getString(R.string.parse_app_id), getString(R.string.parse_client_id));

        ParseUser.enableAutomaticUser();
        ParseACL defaulACL = new ParseACL();

        defaulACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaulACL, true);
    }
}
