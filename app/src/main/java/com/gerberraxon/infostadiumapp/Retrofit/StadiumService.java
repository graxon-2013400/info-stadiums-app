package com.gerberraxon.infostadiumapp.Retrofit;

import com.gerberraxon.infostadiumapp.beans.Stadium;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by graxon on 24/10/15.
 */
public interface StadiumService {

    @GET("/nflapi-static.json")
    void getStadiums(Callback<List<Stadium>> callback);
}
