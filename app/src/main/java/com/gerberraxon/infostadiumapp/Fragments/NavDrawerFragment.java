package com.gerberraxon.infostadiumapp.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.support.v7.widget.Toolbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.RecyclerView;

import com.gerberraxon.infostadiumapp.Adapters.NavDrawerAdapter;
import com.gerberraxon.infostadiumapp.Listeners.RecyclerItemClickListener;
import com.gerberraxon.infostadiumapp.R;
import com.parse.ParseUser;

/**
 * A simple {@link Fragment} subclass.
 */
public class NavDrawerFragment extends Fragment {

    private ActionBarDrawerToggle drawer_toggle;
    private DrawerLayout drawer_layout;
    private Toolbar mToolbar;
    private RecyclerView rcrView;
    private RecyclerView.Adapter adtr;
    private RecyclerView.LayoutManager lytManager;
    private FragmentDrawerListener drawerListener;
    private View contentView;

    private String titulos[] = {"Inicio", "Favoritos", "Acerca"};
    private int iconos[] = {R.drawable.ic_home,R.drawable.ic_favs,R.drawable.ic_about};
    private int PROFILE = R.drawable.my_image;

    public NavDrawerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_nav_drawer, container, false);

        ParseUser us = ParseUser.getCurrentUser();

        rcrView = (RecyclerView) v.findViewById(R.id.nav_recycler);
        rcrView.setHasFixedSize(true);
        lytManager = new LinearLayoutManager(getActivity());
        rcrView.setLayoutManager(lytManager);

        adtr = new NavDrawerAdapter(titulos, iconos, us.getUsername(), us.getEmail(), PROFILE);

        rcrView.setAdapter(adtr);

        rcrView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                drawerListener.onDrawerItemSelected(view, position);
                drawer_layout.closeDrawer(contentView);
            }
        }));

        return v;
    }

    public void setUp(DrawerLayout drawerLayout, Toolbar toolbar, int fragment){
        this.drawer_layout = drawerLayout;
        this.mToolbar = toolbar;
        this.contentView = getActivity().findViewById(fragment);

        drawer_toggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close){

            @Override
            public void onDrawerOpened(View drawerView){
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                mToolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        drawer_layout.setDrawerListener(drawer_toggle);
        drawer_layout.post(new Runnable() {
            @Override
            public void run() {
                drawer_toggle.syncState();
            }
        });
    }

    public void setDrawerListener(FragmentDrawerListener listener){
        this.drawerListener =  listener;
    }

    public interface FragmentDrawerListener {
        void onDrawerItemSelected(View view, int position);
    }
}
