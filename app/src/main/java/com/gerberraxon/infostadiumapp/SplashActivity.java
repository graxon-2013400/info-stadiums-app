package com.gerberraxon.infostadiumapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import com.gerberraxon.infostadiumapp.Body.DashBoardActivity;
import com.gerberraxon.infostadiumapp.Body.LoginActivity;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseUser;


public class SplashActivity extends ActionBarActivity {

    private static final long SPLASH_SCREEN_DELAY = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView titleApp;

        titleApp = (TextView) findViewById(R.id.tv_titleApp);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/ostrich-sans/OstrichSans-Heavy.otf");

        titleApp.setTypeface(font);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent next = null;
                if(ParseAnonymousUtils.isLinked(ParseUser.getCurrentUser())){

                    next = new Intent(SplashActivity.this,LoginActivity.class);
                }else{
                    ParseUser currentUser = ParseUser.getCurrentUser();

                    if(currentUser != null){
                        next = new Intent(SplashActivity.this, DashBoardActivity.class);
                    }else{
                        next = new Intent(SplashActivity.this, LoginActivity.class);
                    }
                }
                SplashActivity.this.startActivity(next);

                finish();
            }
        }, SPLASH_SCREEN_DELAY);
    }
}
