package com.gerberraxon.infostadiumapp.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gerberraxon.infostadiumapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {


    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_about, container, false);

        Typeface ty = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Walkway/Walkway_Oblique_Black.ttf");
        TextView tv_dev_name = (TextView) v.findViewById(R.id.tv_dev_name);
        tv_dev_name.setTypeface(ty);
        return v;
    }


}
