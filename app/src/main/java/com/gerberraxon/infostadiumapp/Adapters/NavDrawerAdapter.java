package com.gerberraxon.infostadiumapp.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gerberraxon.infostadiumapp.R;

/**
 * Created by graxon on 4/11/15.
 */
public class NavDrawerAdapter extends RecyclerView.Adapter<NavDrawerAdapter.ViewHolder>  {

    private static final int TIPO_ENCABEZADO = 0;
    private static final int TIPO_ELEMENTO = 1;

    private String titulos[];
    private int iconos[];

    private String usuario;
    private int perfil;
    private String correo;

    public static class ViewHolder extends RecyclerView.ViewHolder  {
        int holder_id;

        TextView tv_item, tv_user, tv_correo;
        ImageView ic_item, img_perfil;

        public ViewHolder(View view, int tipo){
            super(view);

            if(tipo == TIPO_ELEMENTO){
                tv_item = (TextView) view.findViewById(R.id.tv_item);
                ic_item = (ImageView) view.findViewById(R.id.ic_item);

                holder_id = 1;
            }else{
                tv_user = (TextView) view.findViewById(R.id.user);
                tv_correo = (TextView) view.findViewById(R.id.correo);
                img_perfil = (ImageView) view.findViewById(R.id.img_perfil);

                holder_id = 0;
            }
        }
    }

    public NavDrawerAdapter(String titulos[], int iconos[], String usuario, String correo, int perfil){
        this.titulos = titulos;
        this.iconos = iconos;
        this.usuario = usuario;
        this.correo = correo;
        this.perfil = perfil;
    }

    @Override
    public NavDrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int tipo){
        if(tipo == TIPO_ELEMENTO){
            View vw = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_drawer_item, parent, false);
            ViewHolder vwHolderItm = new ViewHolder(vw, tipo);
            return vwHolderItm;
        }else if(tipo == TIPO_ENCABEZADO){
            View vw = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_drawer_header, parent, false);
            ViewHolder vwHolderItm = new ViewHolder(vw, tipo);
            return vwHolderItm;
        }

        return null;
    }

    @Override
    public void onBindViewHolder(NavDrawerAdapter.ViewHolder holder, int position){

        if(holder.holder_id == 1){
            holder.tv_item.setText(titulos[position-1]);
            holder.ic_item.setImageResource(iconos[position-1]);
        }else{
            holder.img_perfil.setImageResource(perfil);
            holder.tv_user.setText(usuario);
            holder.tv_correo.setText(correo);
        }
    }

    @Override
    public int getItemCount() {
        return titulos.length + 1;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    @Override
    public int getItemViewType(int position){
        if(isPositionHeader(position)){
            return TIPO_ENCABEZADO;
        }
        return TIPO_ELEMENTO;
    }
}
