package com.gerberraxon.infostadiumapp.Body;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.gerberraxon.infostadiumapp.Fragments.HomeFragment;
import com.gerberraxon.infostadiumapp.R;
import com.gerberraxon.infostadiumapp.beans.Stadium;
import com.squareup.picasso.Picasso;

public class StadiumDetail extends ActionBarActivity {

    private Toolbar mToolbar;
    private Stadium stdSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stadium_detail);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Typeface tf_name =  Typeface.createFromAsset(getAssets(), "fonts/Walkway/Walkway_Black.ttf");
        Typeface tf_detail =  Typeface.createFromAsset(getAssets(), "fonts/Walkway/Walkway_SemiBold.ttf");
        Typeface tf_labels =  Typeface.createFromAsset(getAssets(), "fonts/Caviar-Dreams/Caviar_Dreams_Bold.ttf");
        stdSelected = HomeFragment.getStadiumselected();

        ImageView img_std = (ImageView) findViewById(R.id.img_Stadium);
        TextView tv_std_name = (TextView) findViewById(R.id.tv_std_name);
        TextView tv_std_address = (TextView) findViewById(R.id.tv_std_address);
        TextView tv_std_phone = (TextView) findViewById(R.id.tv_std_phone);
        TextView tv_std_state = (TextView) findViewById(R.id.tv_std_state);
        TextView tv_std_city = (TextView) findViewById(R.id.tv_std_city);
        TextView tv_std_description = (TextView) findViewById(R.id.tv_std_description);


        TextView lbl_address = (TextView) findViewById(R.id.lbl_std_address);
        TextView lbl_phone = (TextView) findViewById(R.id.lbl_std_phone);
        TextView lbl_state = (TextView) findViewById(R.id.lbl_std_state);
        TextView lbl_city = (TextView) findViewById(R.id.lbl_std_city);
        TextView lbl_description = (TextView) findViewById(R.id.lbl_std_description);


        tv_std_name.setTypeface(tf_name);
        tv_std_address.setTypeface(tf_detail);
        tv_std_phone.setTypeface(tf_detail);
        tv_std_state.setTypeface(tf_detail);
        tv_std_city.setTypeface(tf_detail);
        tv_std_description.setTypeface(tf_detail);

        lbl_address.setTypeface(tf_labels);
        lbl_phone.setTypeface(tf_labels);
        lbl_state.setTypeface(tf_labels);
        lbl_city.setTypeface(tf_labels);
        lbl_description.setTypeface(tf_labels);


        if(!stdSelected.getImage_url().equals("")){
            Picasso pic = Picasso.with(this);

            pic.setIndicatorsEnabled(true);
            pic.load(stdSelected.getImage_url())
                    .into(img_std);
        }

        tv_std_name.setText(stdSelected.getName());
        tv_std_address.setText(stdSelected.getAddress());
        tv_std_phone.setText(stdSelected.getPhone());
        tv_std_state.setText(stdSelected.getState());
        tv_std_city.setText(stdSelected.getCity());
        tv_std_description.setText(stdSelected.getDescription());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_stadium_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        if(id == android.R.id.home){
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
